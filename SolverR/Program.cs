﻿using System;

namespace SolverR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input A,B to solve A+B.");
            Console.Write("A=");
            int A = int.Parse(Console.ReadLine());
            Console.Write("B=");
            int B = int.Parse(Console.ReadLine());            
            var sumAB = GetSumm(A, B);
            Console.WriteLine("A+B=" + sumAB);
            Console.ReadKey();
        }

        public static int GetSumm(int A,int B)
        {
            return A + B;
        }
    }
}
